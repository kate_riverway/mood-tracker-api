<?php

namespace App\Tests\Functional;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Tests\testTrait;
use Carbon\Carbon;
use Spatie\Snapshots\MatchesSnapshots;

class MoodEstimationTest extends ApiTestCase
{
    use MatchesSnapshots;
    use testTrait;

    public function setUp(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        Carbon::setTestNow('2020-01-01');
        $this->withFixtures(static::$kernel);
        parent::setUp();
    }

    public function testGet()
    {
        $response = static::createClient()->request('GET', '/api/mood_estimations');
        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesJsonSnapshot($response->getContent(false));
    }

    public function testCreate()
    {
        $response = static::createClient()->request('POST', '/api/mood_estimations',
            [
                'json' => [
                    'id' => '1134ed0d-c81b-4dfd-96bb-c8c168115d54',
                    'moodPoints' => 4,
                ],
                'headers' => [
                    'Content-type' => 'application/ld+json',
                    'Accept' => 'application/ld+json',
                ],
            ]);
        $this->assertResponseStatusCodeSame(201);
        $this->assertMatchesJsonSnapshot($response->getContent(false));
    }

}
