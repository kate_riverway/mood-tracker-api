<?php

namespace App\Tests;


use App\DataFixtures\AdminFixtures;
use App\DataFixtures\ChatFixtures;
use App\DataFixtures\MoodEstimationFixtures;
use App\DataFixtures\QuizFixtures;
use App\DataFixtures\QuizzFixtures;
use App\DataFixtures\StudentFixtures;
use App\DataFixtures\SignupTokenFixtures;
use App\DataFixtures\StudentQuizAttemptFixtures;
use App\DataFixtures\TrainerFixtures;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\HttpKernel\KernelInterface;

trait testTrait
{
    protected ?ReferenceRepository $refRepo;
//    protected ?JWTManager $jwtManager;


    protected function getEntity(KernelInterface $kernel, string $entity, string $id)
    {
        $em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $entity = $em->getRepository($entity)->find($id);
        if ($entity) {
            $em->refresh($entity);
        }

        return $entity;
    }

    protected function removeEntity(KernelInterface $kernel, string $entity, string $id)
    {
        /** @var EntityManagerInterface $em */
        $em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $entity = $em->getRepository($entity)->find($id);
        if ($entity) {
            $em->remove($entity);
            $em->flush();
        }

        return $entity;
    }

    protected function withFixtures(KernelInterface $kernel, array $additionalFixtures = [], bool $loadOnlyAdditional = false)
    {
        $em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $encoder = $kernel->getContainer()->get("security.password_encoder");
        $loader = new Loader();
        $loader->addFixture(new MoodEstimationFixtures($encoder));
        $purger = new ORMPurger($em);
        $purger->setPurgeMode($purger::PURGE_MODE_TRUNCATE);
        $executor = new ORMExecutor($em, $purger);
        $executor->execute($loader->getFixtures());
        $this->refRepo = $executor->getReferenceRepository();
    }

//    protected function withJwt(KernelInterface $kernel)
//    {
//        $this->jwtManager = $kernel->getContainer()->get('lexik_jwt_authentication.jwt_manager');
//    }

    protected function withMockedHttpClient(KernelInterface $kernel, array $fixtures)
    {
        $mockResponses = [];
        foreach ($fixtures as $fixture) {
            $mockResponses[] = $fixture instanceof MockResponse ? $fixture : new MockResponse(file_get_contents($fixture));
        }
        $mockResponses[] = new MockResponse('Not accessible', ['http_code' => 500]);

        $client = new MockHttpClient($mockResponses);

        $kernel->getContainer()->set('test.http_client', $client);
    }

    protected function runCommand(KernelInterface $kernel, string $name, array $input = [], array $options = []): string
    {
        $application = new Application($kernel);
        $command = $application->find($name);
        $commandTester = new CommandTester($command);
        $commandTester->execute($input, $options);

        return $commandTester->getDisplay();
    }
}
