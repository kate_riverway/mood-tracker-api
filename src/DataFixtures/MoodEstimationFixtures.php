<?php
namespace App\DataFixtures;

use App\Entity\MoodEstimation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MoodEstimationFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $moodEst = MoodEstimation::createFromFixtures('de0c542c-6d75-4155-8c15-04474a6cc15f', 3);
        $this->addReference('mood_est_1', $moodEst);
        $manager->persist($moodEst);
        $manager->flush();
    }
}
