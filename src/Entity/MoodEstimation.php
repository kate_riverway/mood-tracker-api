<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MoodEstimationRepository;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use App\Dto\CreateMoodEstimationInput;

/**
 * @ApiResource(
 *     attributes={"order"={"estimatedAt": "DESC"}},
 *     collectionOperations={
 *         "get",
 *         "post" = {
 *             "input" = CreateMoodEstimationInput::class,
 *         }
 *     })
 * @ORM\Entity(repositoryClass=MoodEstimationRepository::class)
 */
class MoodEstimation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="guid", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $moodPoints;

    /**
     * @ORM\Column(type="datetime")
     */
    private $estimatedAt;

    public function getId(): string
    {
        return $this->id;
    }

    private function __construct(string $id, int $moodPoints)
    {
        $this->id = $id;
        $this->moodPoints = $moodPoints;
        $this->estimatedAt = Carbon::now()->toDateTimeImmutable();
    }

    public static function createFromFixtures(string $id, int $moodPoints): self {
        return new static($id, $moodPoints);
    }

    public static function createFromInput(CreateMoodEstimationInput $input): self {

        return new static($input->id, $input->moodPoints);
    }

    public function getMoodPoints(): ?int
    {
        return $this->moodPoints;
    }

    public function getEstimatedAt(): ?\DateTimeInterface
    {
        return $this->estimatedAt;
    }

}
