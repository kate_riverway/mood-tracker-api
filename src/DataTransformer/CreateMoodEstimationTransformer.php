<?php
namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\CreateMoodEstimationInput;
use App\Entity\MoodEstimation;

class CreateMoodEstimationTransformer implements DataTransformerInterface
{
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function transform($object, string $to, array $context = [])
    {
        /** @var CreateMoodEstimationInput $object */
        $this->validator->validate($object);

        return MoodEstimation::createFromInput($object);
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof MoodEstimation) {
            return false;
        }
        return MoodEstimation::class === $to
            && null !== ($context['input']['class'] ?? null)
            && $context['input']['class'] === CreateMoodEstimationInput::class;
    }
}
