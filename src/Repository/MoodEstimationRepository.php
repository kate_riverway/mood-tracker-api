<?php

namespace App\Repository;

use App\Entity\MoodEstimation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MoodEstimation|null find($id, $lockMode = null, $lockVersion = null)
 * @method MoodEstimation|null findOneBy(array $criteria, array $orderBy = null)
 * @method MoodEstimation[]    findAll()
 * @method MoodEstimation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MoodEstimationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MoodEstimation::class);
    }

    // /**
    //  * @return MoodEstimation[] Returns an array of MoodEstimation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MoodEstimation
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
